'use strict';

const { hostname, port, address, protocol } = require('./config');
const promisify = require('es6-promisify');
const async = require('async');
const map = promisify(async.map);
const mapSeries = promisify(async.mapSeries);
const { format } = require('url');
const { join } = require('path');
const { writeFileSync } = require('fs');
const Web3 = require('web3');
const ExchangeABI = require('eth-idex/build/contracts/ExchangeWhitelist.sol').abi;
const zipObject = require('lodash/zipObject');
const tokens = require('./tokens');
const uniq = require('lodash/uniq');
const omitBy = require('lodash/omitBy');
const size = require('lodash/size');
const groupBy = require('lodash/groupBy');
const keyBy = require('lodash/keyBy');
const mergeWith = require('lodash/mergeWith');
const mapValues = require('lodash/mapValues');
const BigRational = require('big-rational');
const BigNumber = require('big-number');

const tokenRawMap = keyBy(tokens, 'address');
const tokenMap = mapValues(keyBy(tokens, 'address'), (value) => value.symbol.toUpperCase());

const web3 = new Web3(new Web3.providers.HttpProvider(format({
  hostname,
  port,
  protocol
})));


const { nextTick } = process;

const { sha3, isAddress, toBigNumber } = web3;
const depositEvt = sha3('Deposit(address,address,uint256,uint256)');
const tradeEvt = sha3('Trade(address,uint256,address,uint256,address,address,bytes32)');


if (!isAddress(address)) {
  throw Error('Not an address: ' + address);
}

const exchange = web3.eth.contract(ExchangeABI).at(address);

const getRelevantTransactions = () => new Promise((resolve, reject) => {
  const transactions = [];
  const queueFlush = (() => {
    let isFlushing = false;
    return () => {
      if (!isFlushing) {
        isFlushing = true;
        nextTick(() => filter.stopWatching(() => {
          resolve(transactions);
        }));
      }
    };
  })();
  const filter = web3.eth.filter({
    address,
    fromBlock: 0,
    toBlock: 'latest'
  });
  filter.watch((err, result) => {
    if (err) return console.log(err.stack);
    const { data, topics } = result;
    if (topics && topics[0] && topics[0] === depositEvt) {
      let i = 26;
      const token = '0x' + data.substr(i, 40);
      i += 64;
      const user = '0x' + data.substr(i, 40);
      i += 40;
      const amount = toBigNumber('0x' + data.substr(i, 64)).toPrecision();
      i += 64;
      const balance = toBigNumber('0x' + data.substr(i, 64)).toPrecision();
      transactions.push({
        type: 'deposit',
        token,
        user,
        amount,
        balance
      });
    } else if (topics && topics[0] && topics[0] === tradeEvt) {
      let i = 26;
      const tokenBuy = '0x' + data.substr(i, 40);
      i += 40;
      const amountBuy = toBigNumber('0x' + data.substr(i, 64)).toPrecision();
      i += 88;
      const tokenSell = '0x' + data.substr(i, 40);
      i += 40;
      const amountSell = toBigNumber('0x' + data.substr(i, 64)).toPrecision();
      i += 88;
      const get = '0x' + data.substr(i, 40);
      i += 64;
      const give = '0x' + data.substr(i, 40);
      i += 40;
      const hash = '0x' + data.substr(i, 64);
      transactions.push((function (v) { console.log(require('util').inspect(v, { depth: 15, colors: true })); return v; })({
        tokenBuy,
        amountBuy,
        amountSell,
        tokenSell,
        get,
        give,
        hash,
        type: 'trade'
      }));
    } else { return; }
    queueFlush();
  });
});

const { keys } = Object;

const getBalance = (token, user) => new Promise((resolve, reject) => {
  exchange.tokens.call(token, user, (err, result) => {
    if (err) return reject(err);
    resolve(result.toPrecision());
  });
});

getRelevantTransactions().then((transactions) => {
  return Promise.all([
    Promise.resolve(mapValues(groupBy(transactions.filter((v) => v.type === 'trade'), 'get'), (value) => uniq(value.map((v) => v.tokenSell)))),
    Promise.resolve(mapValues(groupBy(transactions.filter((v) => v.type === 'deposit'), 'user'), (value) => uniq(value.map((v) => v.token))))
  ]).then(([ trades, deposits ]) => {
    console.log(require('util').inspect(trades, { colors: true, depth: 15 }));
    console.log(require('util').inspect(deposits));
    return mergeWith(trades, deposits, (objValue, srcValue) => {
      if (Array.isArray(objValue)) return objValue.concat(srcValue);
    });
  });
}).then((accounts) => {
  return mapValues(accounts, (value) => uniq(value));
}).then((accounts) => {
  return mapSeries(keys(accounts), (a, next) => {
    const tokenSymbols = accounts[a].map((v) => tokenMap[v]);
    map(accounts[a], (v, next) => {
      getBalance(v, a).then((balance) => {
        next(null, BigRational(balance).divide(BigNumber(10).pow(tokenRawMap[v].decimals)).toDecimal());
      }).catch(next);
    }).then((balances) => {
      next(null, zipObject(tokenSymbols, balances));
    }).catch((err) => {
      next(err);
    });
  }).then((balanceMaps) => zipObject(keys(accounts), balanceMaps));
}).then((balances) => {
  return mapValues(balances, (value) => {
    return omitBy(value, (value) => {
      return BigRational(value).eq(0);
    });
  });
}).then((omitted) => {
  return omitBy(omitted, (value) => {
    return !keys(value).length;
  });
}).then((balances) => {
  console.log(require('util').inspect(balances, { colors: true, depth: 15 }));
  writeFileSync(join(process.cwd(), 'balances.json'), JSON.stringify(balances, null, 2));
});
    
